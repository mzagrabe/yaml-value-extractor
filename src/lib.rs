// {{{ use
use serde_yaml::{
    Index,
    Value,
};
// }}} use


// {{{ #[cfg(test)] mod tests
#[cfg(test)]
mod tests {
    use super::*;
    use serde_yaml::from_str;

    // {{{ #[test] fn yaml_selector_parser_simple
    #[test]
    fn yaml_selector_parser_simple() {
        assert_eq!(
            _yaml_selector_parse("foo"),
            vec![
                "foo".to_owned(),
            ],
        );
    }
    // }}} #[test] fn yaml_selector_parser_simple


    // {{{ #[test] fn yaml_selector_parser_multiple
    #[test]
    fn yaml_selector_parser_multiple() {
        assert_eq!(
            _yaml_selector_parse("foo[bar][baz]"),
            vec![
                "foo".to_owned(),
                "bar".to_owned(),
                "baz".to_owned(),
            ],
        );
    }
    // }}} #[test] fn yaml_selector_parser_multiple


    // {{{ #[test] fn yaml_selector_parser_digits
    #[test]
    fn yaml_selector_parser_digits() {
        assert_eq!(
            _yaml_selector_parse("foo[0][baz]"),
            vec![
                "foo".to_owned(),
                "0".to_owned(),
                "baz".to_owned(),
            ],
        );
    }
    // }}} #[test] fn yaml_selector_parser_digits


    // {{{ #[test] fn yaml_value_extract_simple
    #[test]
    fn yaml_value_extract_simple() {
        assert_eq!(
            yaml_value_extract(
                &from_str(
                    "foo: bar"
                )
                    .unwrap()
                ,
                "foo",
            ),
            Ok(Some("bar".to_owned())),
        );
    }
    // }}} #[test] fn yaml_value_extract_simple


    // {{{ #[test] fn yaml_value_extract_simple_bool
    #[test]
    fn yaml_value_extract_simple_bool() {
        assert_eq!(
            yaml_value_extract(
                &from_str(
                    "foo: true"
                )
                    .unwrap()
                ,
                "foo",
            ),
            Ok(Some("true".to_owned())),
        );
    }
    // }}} #[test] fn yaml_value_extract_simple_bool


    // {{{ #[test] fn yaml_value_extract_simple_int_to_string
    #[test]
    fn yaml_value_extract_simple_int_to_string() {
        assert_eq!(
            yaml_value_extract(
                &from_str(
                    "foo: 123"
                )
                    .unwrap()
                ,
                "foo",
            ),
            Ok(Some("123".to_owned())),
        );
    }
    // }}} #[test] fn yaml_value_extract_simple_int_to_string


    // {{{ #[test] fn yaml_value_extract_simple_float_to_string
    #[test]
    fn yaml_value_extract_simple_float_to_string() {
        assert_eq!(
            yaml_value_extract(
                &from_str(
                    "foo: 1.23"
                )
                    .unwrap()
                ,
                "foo",
            ),
            Ok(Some("1.23".to_owned())),
        );
    }
    // }}} #[test] fn yaml_value_extract_simple_float_to_string


    // {{{ #[test] fn yaml_value_extract_simple_null_to_string
    #[test]
    fn yaml_value_extract_simple_null_to_string() {
        assert_eq!(
            yaml_value_extract(
                &from_str(
                    "foo: null"
                )
                    .unwrap()
                ,
                "foo",
            ),
            Ok(Some("null".to_owned())),
        );
    }
    // }}} #[test] fn yaml_value_extract_simple_null_to_string


    // {{{ #[test] fn yaml_value_extract_simple_nonexistent
    #[test]
    fn yaml_value_extract_simple_nonexistent() {
        assert_eq!(
            yaml_value_extract(
                &from_str(
                    "foo: bar"
                )
                    .unwrap()
                ,
                "baz",
            ),
            Ok(None),
        );
    }
    // }}} #[test] fn yaml_value_extract_simple_nonexistent


    // {{{ #[test] fn yaml_value_extract_with_array
    #[test]
    fn yaml_value_extract_with_array() {
        assert_eq!(
            yaml_value_extract(
                &from_str(
                    r#"foo:
    - bar
    - baz
"#
                )
                    .unwrap()
                ,
                "foo[0]",
            ),
            Ok(Some("bar".to_owned())),
        );
    }
    // }}} #[test] fn yaml_value_extract_with_array


    // {{{ #[test] fn yaml_value_extract_with_array_nonexistent
    #[test]
    fn yaml_value_extract_with_array_nonexistent() {
        assert_eq!(
            yaml_value_extract(
                &from_str(
                    r#"foo:
    - bar
    - baz
"#
                )
                    .unwrap()
                ,
                "foo[2]",
            ),
            Ok(None),
        );
    }
    // }}} #[test] fn yaml_value_extract_with_array_nonexistent


    // {{{ #[test] fn yaml_value_extract_with_array_of_hashes
    #[test]
    fn yaml_value_extract_with_array_of_hashes() {
        assert_eq!(
            yaml_value_extract(
                &from_str(
                    r#"foo:
    -
        bar: baz
        baz: qux
    -
        bar: foo
        baz: baz
"#
                )
                    .unwrap()
                ,
                "foo[1][baz]",
            ),
            Ok(Some("baz".to_owned())),
        );
    }
    // }}} #[test] fn yaml_value_extract_with_array_of_hashes
}
// }}} #[cfg(test)] mod tests


// {{{ fn _yaml_selector_parse
fn _yaml_selector_parse(
    selector: &str,
) -> Vec<&str> {
    selector
        .split('[')
        .map(
            |x| x.trim_end_matches(']')
        )
        .collect::<Vec<&str>>()
}
// }}} fn _yaml_selector_parse


// {{{ pub fn yaml_value_extract
pub fn yaml_value_extract<'a>(
    deserialized_yaml: &'a Value,
    selector:          &'a str,
) -> Result<Option<String>, std::num::ParseIntError> {
    let indices = _yaml_selector_parse(selector);

    let mut current_depth = deserialized_yaml;

    for indice in indices {
        let index: &dyn Index;
        let index_as_int: usize;
        if current_depth.is_sequence() {
            index_as_int = indice.parse()?;
            index = &index_as_int;
        }
        else {
            index = &indice;
        }
        current_depth = match current_depth.get(index) {
            Some(current_depth) => current_depth,
            None                => {
                return Ok(None);
            }
        };
    }

    if current_depth.is_bool() {
        match current_depth.as_bool() {
            Some(boolean) => return Ok(Some(boolean.to_string())),
            None          => return Ok(None),
        }
    }
    if current_depth.is_f64() {
        match current_depth.as_f64() {
            Some(number) => return Ok(Some(number.to_string())),
            None         => return Ok(None),
        }
    }
    if current_depth.is_i64() {
        match current_depth.as_i64() {
            Some(number) => return Ok(Some(number.to_string())),
            None         => return Ok(None),
        }
    }
    if current_depth.is_u64() {
        match current_depth.as_u64() {
            Some(number) => return Ok(Some(number.to_string())),
            None         => return Ok(None),
        }
    }
    if current_depth.is_null() {
        match current_depth.as_null() {
            Some(()) => return Ok(Some("null".to_owned())),
            None     => return Ok(None),
        }
    }
    match current_depth.as_str() {
        Some(value) => Ok(Some(value.to_string())),
        None        => Ok(None),
    }
}
// }}} pub fn yaml_value_extract
