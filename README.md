## Name
YAML Value Extractor

## Description
Given a deserialized (Serde) YAML value and a selector, return an Option<String> from the YAML document.

## Installation
cargo build --release

## Usage
```
use serde_yaml::from_str;
use yaml_value_extractor::yaml_value_extract;

let bar = yaml_value_extract(
    &from_str(
        "foo: bar",
    ).unwrap(),
    "foo",
);
````

See src/lib.rs unit tests for further examples.

## Support
Submit an issue if you have questions or comments.

## Authors and acknowledgment
Matt Zagrabelny <mzagrabe@d.umn.edu>
Many thanks to the Rust community.

## License
GPLv3 or later
